import { useEffect } from "react";
import { Link, useLoaderData, json } from "react-router-dom";
import avatar from "../images/no-avatar.png";
import classes from "./Details.module.css";

const Details = () => {
  const data = useLoaderData();
  const title = data.name ? `${data.name} – Rick and Morty` : "Rick and Morty";
  
  useEffect(() => {
    document.title = title;
  }, [title]);

  return (
    <section className={classes.about}>
      <div className="container">
        <Link to="/" className={classes["about-back"]}>
          go back
        </Link>
        <div className={classes["about-general"]}>
          <img
            src={data.image ? data.image : avatar}
            alt={data.name}
            className={classes["about-general__avatar"]}
          />
          <h1 className={classes["about-general__name"]}>{data.name}</h1>
        </div>
        <div className={classes["about-description"]}>
          <h5 className={classes["about-description__title"]}>Informations</h5>
          <ul className={classes["about-items"]}>
            <li className={classes["about-item"]}>
              <span className={classes["about-item__name"]}>Gender</span>
              <span className={classes["about-item__value"]}>{data.gender ? data.gender : "Unknown"}</span>
            </li>
            <li className={classes["about-item"]}>
              <span className={classes["about-item__name"]}>Status</span>
              <span className={classes["about-item__value"]}>{data.status ? data.status : "Unknown"}</span>
            </li>
            <li className={classes["about-item"]}>
              <span className={classes["about-item__name"]}>Specie</span>
              <span className={classes["about-item__value"]}>{data.species ? data.species : "Unknown"}</span>
            </li>
            <li className={classes["about-item"]}>
              <span className={classes["about-item__name"]}>Origin</span>
              <span className={classes["about-item__value"]}>{data.origin.name ? data.origin.name : "Unknown"}</span>
            </li>
            <li className={classes["about-item"]}>
              <span className={classes["about-item__name"]}>Type</span>
              <span className={classes["about-item__value"]}>{data.type ? data.type : "Unknown"}</span>
            </li>
          </ul>
        </div>
      </div>
    </section>
  );
};

export default Details;

export const loader = async ({ params }) => {
  const id = params.id;

  if (!id) {
    throw json({ message: "ID is required" }, { status: 404 });
  }

  const response = await fetch(
    `https://rickandmortyapi.com/api/character/${id}`
  );

  if (!response.ok) {
    throw json({ message: "Error while loading" }, { status: 500 });
  }

  return response;
};
