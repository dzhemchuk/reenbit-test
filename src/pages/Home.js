import Characters from "../components/Home/Characters";
import Header from "../components/Home/Header";
import { json, useLoaderData } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { characterActions } from "../storage/characters";
import { statesActions } from "../storage/states";
import { useEffect } from "react";

const Home = () => {
    const data = useLoaderData();
    const dispatch = useDispatch();

    const isInitial = useSelector(state => state.states.initial);

    if(isInitial){
        dispatch(characterActions.setCharacters(data));
        dispatch(statesActions.setInitial(false));
    }
    
    useEffect(() => {
        document.title = "Rick and Morty";
    }, []);

    return <>
        <Header />
        <Characters />
    </>;
}

export default Home;

export const loader = async () => {
    let storage = localStorage.getItem("characters");
    
    if(storage){
        storage = JSON.parse(storage);
        storage.clear = true;
        return storage;
    }
    else{
        const response = await fetch("https://rickandmortyapi.com/api/character");
  
        if (!response.ok) {
          throw json({ message: "Error while loading data" }, { status: 500 });
        }
        
        let data = await response.json();
        data = {
            characters: data.results ? data.results : [],
            next: data.info.next ? data.info.next : null,
            search: ""
        };
        
        return data;
    }
};