import { useEffect } from "react";
import { useLoaderData, useNavigate } from "react-router-dom";

const Auth = () => {
  const data = useLoaderData();
  const navigate = useNavigate();
  const token = data.access_token;

  useEffect(() => {
    if (token) {
      localStorage.setItem("token", token);
      navigate("/");
    }
  }, [token, navigate]);

  useEffect(() => {
    document.title = "Authorization – Rick and Morty";
  }, []);

  return <>{data.error && <h1>{data.error}</h1>}</>;
};

export default Auth;

export const loader = async ({ request }) => {
  const searchParams = new URL(request.url).searchParams;
  let code = searchParams.get("code");

  if (!code) {
    return { error: "Auth code is required" };
  }

  //I decided not to hide this logic behind a backend part. This is just a dummy FRONTEND test project,
  //so it can be forgiven, I guess...

  const requestBody = new URLSearchParams({
    code: code,
    client_id:
      "487329407621-55jjqe4o2gvmp83up3vcvvdi2i409qd5.apps.googleusercontent.com",
    client_secret: "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",
    redirect_uri: "https://reenbit.dzhemchuk.pp.ua/auth",
    grant_type: "authorization_code",
  });

  const response = await fetch(
    "https://www.googleapis.com/oauth2/v4/token?" + requestBody.toString(),
    {
      method: "POST",
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
      },
    }
  );

  const authToken = await response.json();

  return authToken;
};
