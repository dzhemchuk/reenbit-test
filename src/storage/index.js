import { configureStore } from "@reduxjs/toolkit";
import charactersReducers from "./characters";
import statesReducers from "./states";

const store = configureStore({
    reducer: {
        characters: charactersReducers,
        states: statesReducers,
    }
});

export default store;