import { createSlice } from "@reduxjs/toolkit";

const statesSlice = createSlice({
    name: "states",
    initialState: {
        error: false,
        initial: true,
    },
    reducers: {
        setError(state, data){
            state.error = data.payload;
        },
        setInitial(state, data){
            state.initial = data.payload;
        },
    }
});

export const statesActions = statesSlice.actions;

export default statesSlice.reducer;