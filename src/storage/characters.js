import { createSlice } from "@reduxjs/toolkit";

const characterSlice = createSlice({
  name: "characters",
  initialState: {
    characters: [],
    next: null,
    search: "",
  },
  reducers: {
    setCharacters(state, data) {
      const oldCharacters = state.characters;
      let newCharacters = data.payload.characters;
      
      newCharacters = newCharacters.sort(function (a, b) {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });

      state.characters = data.payload.clear
        ? newCharacters
        : oldCharacters.concat(newCharacters);

      state.next = data.payload.next;
      // state.search = data.payload.search;x
      
      if (data.payload.search != null){
        state.search = data.payload.search;
      }

      localStorage.setItem("characters", JSON.stringify(state));
    },
    setSearch(state, data) {
      state.search = data.payload;
    },
  },
});

export const characterActions = characterSlice.actions;

export default characterSlice.reducer;
