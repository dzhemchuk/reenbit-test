import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Details from "./pages/Details";
import Home from "./pages/Home";
import { loader as detailsLoader } from "./pages/Details";
import { loader as homeLoader } from "./pages/Home";
import { loader as authLoader } from "./pages/Auth";
import Auth from "./pages/Auth";

const router = createBrowserRouter([
  {
    path: "/",
    errorElement: <h1>Error while loading main page</h1>,
    element: <Home />,
    loader: homeLoader,
    title: "Rick and Morty"
  },
  {
    path: "/details/:id",
    errorElement: <h1>Error while loading character's description</h1>,
    element: <Details />,
    loader: detailsLoader,
  },
  {
    path: "/auth",
    errorElement: <h1>Error while loading auth page</h1>,
    element: <Auth />,
    loader: authLoader,
    title: "Authorization – Rick and Morty"
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
