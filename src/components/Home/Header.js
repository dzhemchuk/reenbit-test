import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import logo from "../../images/logo.png";
import classes from "./Header.module.css";
import { characterActions } from "../../storage/characters";
import { statesActions } from "../../storage/states";

const Header = () => {
  const dispatch = useDispatch();
  const defaultSearch = useSelector((state) => state.characters.search);
  const [search, setSearch] = useState(defaultSearch);
  const [isLoading, setIsLoading] = useState(false);
  const [isEntered, setIsEntered] = useState(false);
  const [isAuthorized, setIsAuthorized] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("token");

    const getEmail = async () => {
      const response = await fetch(
        "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + token
      );

      const data = await response.json();

      if (data.email) {
        setIsAuthorized(data.email);
      } else {
        localStorage.removeItem("token");
        setIsAuthorized(false);
      }
    };

    if (token) {
      getEmail();
    }
  }, [isAuthorized]);

  useEffect(() => {
    const loadData = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(
          `https://rickandmortyapi.com/api/character/?name=${search}`
        );

        if (!response.ok) {
          throw new Error({ message: "Error while loading data" });
        }

        let data = await response.json();

        data = {
          characters: data.results ? data.results : [],
          next: data.info.next ? data.info.next : null,
          clear: true,
          search,
        };

        dispatch(characterActions.setCharacters(data));
        setIsLoading(false);
        dispatch(statesActions.setError(false));
      } catch (error) {
        console.log(error);
        setIsLoading(false);
        dispatch(statesActions.setError(true));
      }
    };

    const delayDebounceFn = setTimeout(() => {
      if (isEntered) {
        setIsEntered(false);
        loadData();
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [search, dispatch, isEntered]);

  const setSearchHandler = (e) => {
    setSearch(e.target.value.trim());
    setIsEntered(true);
  };

  return (
    <header className={classes.header}>
      <div className="container">
        <div className={classes["header-login"]}>
          {isAuthorized ? (
            <p>Hello, {isAuthorized}</p>
          ) : (
            <a
              href="https://accounts.google.com/o/oauth2/auth?redirect_uri=https://reenbit.dzhemchuk.pp.ua/auth&response_type=code&client_id=487329407621-55jjqe4o2gvmp83up3vcvvdi2i409qd5.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email"
              className={classes["header-login__login"]}
              rel="nofollow noopener noreferrer"
              target="_blank"
            >
              Login in with
            </a>
          )}
        </div>
        <Link to="/" className={classes.logo}>
          <img src={logo} alt="Rick & Morty" className={classes.logo__src} />
        </Link>
        <div
          className={
            isLoading && search !== ""
              ? `${classes.search} ${classes.search_loading}`
              : classes.search
          }
        >
          <input
            type="text"
            className={classes.search__field}
            placeholder="Filter by name..."
            onInput={setSearchHandler}
            value={search}
          />
        </div>
      </div>
    </header>
  );
};

export default Header;
