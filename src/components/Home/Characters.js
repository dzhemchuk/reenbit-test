import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import classes from "./Characters.module.css";
import avatar from "../../images/no-avatar.png";
import { useEffect, useState } from "react";
import { characterActions } from "../../storage/characters";
import { json } from "react-router-dom";

const Characters = () => {
  const characters = useSelector((state) => state.characters.characters);
  const next = useSelector((state) => state.characters.next);
  const isError = useSelector((state) => state.states.error);
  const [isLoading, setIsLoading] = useState(false);
  const [loadMore, setLoadMore] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    const loader = async () => {
      setIsLoading(true);
      const response = await fetch(next);

      if (!response.ok) {
        setIsLoading(false);
        setLoadMore(false);
        throw json({ message: "Error while loading data" }, { status: 500 });
      }

      let data = await response.json();
      data = {
        characters: data.results ? data.results : [],
        next: data.info.next ? data.info.next : null,
      };

      dispatch(characterActions.setCharacters(data));
      setIsLoading(false);
    };

    if(loadMore){
      setLoadMore(false);
      loader();
    }
    
  }, [dispatch, loadMore, next]);

  const loadMoreHandler = () => {
    setLoadMore(true);
  }


  return (
    <section className={classes.characters}>
      <div className="container">
        <div className={classes["characters-wrapper"]}>
          {characters.length === 0 || isError ? (
            <p style={{ textAlign: "center", width: "100%" }}>
              No characters found.
            </p>
          ) : (
            characters.map((item) => (
              <Link
                to={`/details/${item.id}`}
                className={classes.character}
                key={item.id}
                title={
                  item.name && item.image !== ""
                    ? item.name
                    : "No name provided"
                }
              >
                <img
                  src={item.image && item.image !== "" ? item.image : avatar}
                  alt={
                    item.name && item.image !== ""
                      ? item.name
                      : "No name provided"
                  }
                  className={classes.character__avatar}
                />
                <div className={classes["character-info"]}>
                  <h5 className={classes["character-info__name"]}>
                    {item.name && item.image !== ""
                      ? item.name
                      : "No name provided"}
                  </h5>
                  <span className={classes["character-info__type"]}>
                    {item.species && item.species !== ""
                      ? item.species
                      : "No type provided"}
                  </span>
                </div>
              </Link>
            ))
          )}
          {isLoading && (
            <p style={{ textAlign: "center", width: "100%" }}>Loading...</p>
          )}
        </div>
        {next && !isError && (
          <button
            type="button"
            className={classes["characters-more"]}
            onClick={loadMoreHandler}
          >
            Load more
          </button>
        )}
      </div>
    </section>
  );
};

export default Characters;
